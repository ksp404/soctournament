"use strict";
const winston = require('winston');

const ENV = process.env.NODE_ENV;

function getLogger(module) {
    const tsFormat = () => (new Date()).toLocaleTimeString();

    let path = module.filename.split('/').slice(-2).join('/');

    return new winston.Logger({
        transports: [
            new winston.transports.Console({
                colorize: true,
                timestamp: tsFormat,
                level: ENV === 'development' ? 'debug' : 'error',
                label: path
            }),

            new winston.transports.File({
                filename: 'server.log',
                timestamp: tsFormat,
                level: 'debug',
                label: path
            })
        ],
        exitOnError: false
    });
}

module.exports = getLogger;