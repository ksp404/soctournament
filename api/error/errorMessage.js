module.exports = {
    dbMsgConstraintLess0: 'players_points_check',
    dbMsgConstraintPlayerInTour: 'tournament_player_idx',
    dbMsgConstraintPk: 'tournaments_primary_key',
    playerNotFound: 'player not found',
    notEnoughPoints: 'not have enough points',
    tournamentIdNotFound: "tournamentId not found",
    invalidPoints: 'invalid points',
    tournamentExists: 'tournament exists',
    tournamentNotFound: 'tournament not found',
    backerNotFound: 'backer not found',
    tournamentEnded: 'the tournament has ended',
    inviteYourself: 'invite yourself is pointless',
    winnerNotFound: 'winners not found',
    plaerNotParticipate: 'not participate in the tournament.',
    playerParticipatesTour: 'Player participates in the tournament',
    playerIdNotSpecified: 'player id is not specified'
}