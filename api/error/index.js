'use strict';
const util = require('util');
const http = require('http');

function AppError(status, message) {
    Error.apply(this, arguments);
    Error.captureStackTrace(this, AppError);
    this.status = status;
    this.message = message || http.STATUS_CODES[status] || "Error";
}

util.inherits(AppError, Error);
AppError.prototype.name = 'AppError';
module.exports = AppError;