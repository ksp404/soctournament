"use strict";
const express = require('express');
const playerLogic = require('../logic/playerLogic');
const errMsg = require('../error/errorMessage');
const log = require('../log')(module);
const tournamentLogic = require('../logic/tournamentLogic');
const dbLogic = require('../logic/dbLogic');
const AppError = require('../error/index');

module.exports = function (app) {

    app.get('/', (req, res) => {
        res.sendFile('index');
    });

    app.get('/balance', (req, res, next) => {
        if (!req.query.playerid)
            return next(new AppError(400, errMsg.playerNotFound));
        playerLogic.balance(req.query.playerid).then(data => {
            return res.json(data);
        }).catch(err => {
            return next(err);
        });
    });

    app.get('/take', (req, res, next) => {
        if (!req.query.playerid)
            return next(new AppError(400, errMsg.playerIdNotSpecified));

        if (!req.query.points || req.query.points < 0)
            return next(new AppError(400, errMsg.invalidPoints));

        playerLogic.take(req.query.playerid, req.query.points)
            .then(player => { return res.json(player) })
            .catch(err => { return next(err) });
    });

    app.get('/fund', (req, res, next) => {
        if (!req.query.playerid)
            return next(new AppError(400, errMsg.playerNotFound));

        if (!req.query.points || req.query.points < 0)
            return next(new AppError(400, errMsg.invalidPoints));

        playerLogic.fund(req.query.playerid, req.query.points)
            .then(player => { return res.json(player) })
            .catch(err => { return next(err) });
    });

    app.get('/joinTournament', (req, res, next) => {
        if (!req.query.tournamentid)
            return next(new AppError(404, errMsg.tournamentIdNotFound));

        if (!req.query.playerid)
            return next(new AppError(400, errMsg.invalidPoints));

        let backers = [];
        if (req.query.backerid)
            Array.isArray(req.query.backerid) ? backers = req.query.backerid : backers.push(req.query.backerid);

        tournamentLogic.joinTournament(req.query.tournamentid, req.query.playerid, backers)
            .then(r => { 
                return res.json(r) 
            })
            .catch(err => { 
                return next(err) 
            });

    });

    app.get('/reset', (req, res) => {
        dbLogic.resetDb()
            .then(r => { return res.json(r) })
            .catch(err => { return next(err) })
    });

    app.post('/resultTournament', (req, res, next) => {
        if (!req.body.tournamentId) return next(new AppError(400, errMsg.tournamentIdNotFound));
        if (!Array.isArray(req.body.winners)) return next(new AppError(400, errMsg.winnerNotFound));
        if (req.body.winners.length == 0) return next(new AppError(400, errMsg.winnerNotFound));

        tournamentLogic.resultTournament(req.body).then(r=>{
            return res.json(r)
        }).catch(err=>{
            return next(err);
        });
    });

    app.get('/announceTournament', (req, res, next) => {
        if (!req.query.tournamentid)
            return next(new AppError(404, errMsg.tournamentIdNotFound));

        if (!req.query.deposit || req.query.deposit <= 0)
            return next(new AppError(400, errMsg.invalidPoints));

        tournamentLogic.announceTournament(req.query.tournamentid, req.query.deposit)
            .then(t => { 
                return res.json(t) 
            })
            .catch(err => { return next(err) })
    });
}