'use strict';
const promise = require('bluebird');
const config = require('../config');


let repos = {
    repPlayer: require('./repos/repPlayer'),
    repTournament: require('./repos/repTournament'),
    repDB: require('./repos/repDB')
};

let options = {

    promiseLib: promise,

    extend: obj => {
        obj.db= repos.repDB(obj, pgp);
        obj.Player = repos.repPlayer(obj, pgp);
        obj.Tournament = repos.repTournament(obj, pgp);
    }
};

// Database connection parameters:
let DBconfig = {
    host: config.get('pg:host'),
    port: config.get('pg:port'),
    database: config.get('pg:database'),
    user: config.get('pg:user'),
    password: config.get('pg:password')
};

let pgp = require('pg-promise')(options);

let db = pgp(process.env.DATABASE_URL||DBconfig);
pgp.pg.defaults.poolSize = config.get('pg:pool');

module.exports = {
    repos,
    pgp,
    db,

};