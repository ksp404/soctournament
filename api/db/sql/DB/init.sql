
CREATE TABLE IF NOT EXISTS ${schema~}.players (
    id VARCHAR(30) NOT NULL,
    points NUMERIC,
    CONSTRAINT players_pkey PRIMARY KEY(id),
    CONSTRAINT players_points_check CHECK (points >= (0)::numeric)
)WITH (OIDS=FALSE);


CREATE TABLE IF NOT EXISTS ${schema~}.tournaments (
  id VARCHAR(30) NOT NULL,
  deposit NUMERIC(15,2),
  is_active boolean DEFAULT TRUE,
  CONSTRAINT tournaments_primary_key PRIMARY KEY(id)
)
WITH (oids = false);


CREATE TABLE IF NOT EXISTS ${schema~}.tournament_player (
  id SERIAL,
  player_id VARCHAR(30) NOT NULL,
  tournament_id VARCHAR(30) NOT NULL,
  points NUMERIC(15,2) NOT NULL,
  CONSTRAINT tournament_player_idx UNIQUE(player_id, tournament_id)
)
WITH (oids = false);

CREATE TABLE IF NOT EXISTS ${schema~}.backers (
  id SERIAL,
  tournament_player_id INTEGER,
  player_id VARCHAR(30) NOT NULL,
  points NUMERIC(15,2),
  CONSTRAINT backer_pkey PRIMARY KEY(id)
)
WITH (oids = false);
