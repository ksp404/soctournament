'use strict';

const QueryFile = require('pg-promise').QueryFile;
const path = require('path');
const config = require('../../config');


// Helper for linking to external query files;
function sql(file) {

    let fullPath = path.join(__dirname, file); 

    let options = {
        minify: true,
        params: {
            schema: config.get('pg:schema')  // replace ${schema~} with config(postgreSQL:schema)
        }
    };
    return new QueryFile(fullPath, options);
}

module.exports = {
    Backer: {},
    DB: {
        init: sql('DB/init.sql'),
        reset: sql('DB/reset.sql'),        
    },
    Player: {
        create: sql('Player/create.sql'),
        delete: sql('Player/delete.sql'),
        selectById: sql('Player/selectById.sql'),
        update: sql('Player/update.sql'),
        changePoints: sql('Player/changePoints.sql'),
        selectPlayersByTournament: sql('Tournament/selectPlayersByTournament.sql'),
        selectBackersByTournament: sql('Tournament/selectBackersByTournament.sql'),
    },
    Tournament: {
        create: sql('Tournament/create.sql'),
        selectById: sql('Tournament/selectById.sql'),
        closeTournament: sql('Tournament/closeTournament.sql'),
        addPlayerToTournament: sql('Tournament/createPlayerTornament.sql'),
        addBacker: sql('Tournament/addBakers.sql'),
    }
};