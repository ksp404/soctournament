insert into ${schema~}.players as p values ($1, $2)
ON CONFLICT (id) DO 
UPDATE  set points = p.points+$2
returning *