"use strict";
const sql = require('../sql').Player;

module.exports = (rep, pgp) => {
    return {
        create: (id, points) => rep.oneOrNone(sql.create, [id, points]),
        selectById: (id) =>  rep.oneOrNone(sql.selectById, [id]),
        changePoints: (id, points) => rep.oneOrNone(sql.changePoints, [id, points]),
        selectPlayersByTournament: (id) => rep.any(sql.selectPlayersByTournament, [id]),
        selectBackersByTournament: (id) => rep.any(sql.selectBackersByTournament, [id]),
    };
};