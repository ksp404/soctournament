'use strict';
const sql = require('../sql').DB;

module.exports = (rep, pgp) => {
    return {
        init: () => rep.any(sql.init),
        resetDb:()=> rep.none(sql.reset)
    };
};