"use strict";
const sql = require('../sql');
const AppError = require('../../error/index');
const errMsg = require('../../error/errorMessage');

module.exports = (rep, pgp) => {
    return {
        create: (id, deposit) => rep.oneOrNone(sql.Tournament.create, [id, deposit]),
        closeTournament: (id) => rep.oneOrNone(sql.Tournament.closeTournament, [id]),
        selectById: (id) =>  rep.oneOrNone(sql.Tournament.selectById, [id]),
        joinTournament:(data, db)=>{
            return db.tx(t => {
                return t.one(sql.Tournament.addPlayerToTournament, [data.player.id, data.tournament.id, data.player.rate]).then(tp=>{
                    let batchData = [];
                    batchData.push(t.oneOrNone(sql.Player.changePoints, [data.player.id, -data.player.rate]));
                    for (let i = 0; i < data.backers.length; i++) {
                        batchData.push(t.oneOrNone(sql.Player.changePoints, [data.backers[i].id, -data.backers[i].rate]));
                        batchData.push(t.oneOrNone(sql.Tournament.addBacker, [tp.id, data.backers[i].id, data.backers[i].rate]));
                    }
                    return t.batch(batchData);
                }).catch(err=>{
                    throw err;
                });
            })
        },
        closeTournament:(data, db)=>{
            return db.tx(t=>{
                return t.oneOrNone(sql.Tournament.closeTournament, [data.tournament.id]).then(tournament=>{
                    if(!tournament)
                        throw new AppError(400, errMsg.tournamentEnded);
                    let batchData = [];
                    data.winners.forEach(function(w) {
                        batchData.push(t.oneOrNone(sql.Player.changePoints, [w.id,w.rate]));
                    });
                    return t.batch(batchData);
                }).catch(err=>{
                    throw err;
                })
            });
        }
    };
};