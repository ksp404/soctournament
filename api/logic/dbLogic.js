"use strict";
const AppError = require('../error/index');
const db = require('../db').db.db;


class DBLogic {
    static resetDb() {
        return db.resetDb().then(() => {
            return Promise.resolve('db reset done');
        }).catch((err) => {
            return Promise.reject(err);
        });
    }
}

module.exports = DBLogic;