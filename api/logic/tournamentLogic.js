"use strict";
const AppError = require('../error/index');
const dbTornament = require('../db').db.Tournament;
const dbPlayer = require('../db').db.Player;
const log = require('../log')(module);
const db = require('../db').db;
const errMsg = require('../error/errorMessage')

class TournamentLogic {

    static announceTournament(id, deposit) {
        return dbTornament.create(id, deposit).then((t) => {
            return Promise.resolve(t);
        }).catch((err) => {
            if (err.constraint == errMsg.dbMsgConstraintPk) //id exists
                err = new AppError(400, errMsg.tournamentExists);

            return Promise.reject(err);
        })
    }

    static joinTournament(id, playerId, backers) {
        var tournamentInfo = {};
        let promises = [];
        promises.push(dbTornament.selectById(id));
        promises.push(dbPlayer.selectById(playerId));
        if (backers && backers.length > 0) {
            for (let i = 0; i < backers.length; i++) {
                promises.push(dbPlayer.selectById(backers[i]));
            }
        }
        return Promise.all(promises).then((result) => {
            if (!result[0])
                throw new AppError(404, errMsg.tournamentNotFound);
            if (!result[1])
                throw new AppError(404, errMsg.playerNotFound);
            tournamentInfo.tournament = result[0];
            tournamentInfo.player = result[1];
            tournamentInfo.backers = [];
            for (let i = 2; i < result.length; i++) {
                if (!result[i])
                    throw new AppError(404, errMsg.backerNotFound);
                tournamentInfo.backers.push(result[i]);
            }

            TournamentLogic.validateJoinTournament(tournamentInfo);
            return dbTornament.joinTournament(tournamentInfo, db);
        }).then(data => {
            return Promise.resolve("ok");
        }).catch((err) => {
            if (err.first && err.first.constraint === errMsg.dbMsgConstraintLess0) // points less 0
                err = new AppError(400, errMsg.notEnoughPoints);
            if (err.constraint == errMsg.dbMsgConstraintPlayerInTour) {
                err = new AppError(400, errMsg.playerParticipatesTour);
            }
            return Promise.reject(err);
        });
    }

    static validateJoinTournament(data) {
        if (!data.tournament.is_active) throw new AppError(400, errMsg.tournamentEnded);
        if (data.backers.filter(b => b.id === data.player.id).length > 0)
            throw new AppError(400, errMsg.inviteYourself)
        let rate = Number(data.tournament.deposit);
        if (data.backers.length > 0) {
            rate = data.tournament.deposit / (data.backers.length + 1);
        }
        if (data.player.points < rate) {
            throw new AppError(400, errMsg.notEnoughPoints + ' playerId = ' + data.player.id)
        }
        data.player.rate = rate;
        for (let i = 0; i < data.backers.length; i++) {
            if (data.backers[i].points < rate) {
                throw new AppError(400, 'Backer' + data.backers[i].id + errMsg.notEnoughPoints)
            }
            data.backers[i].rate = rate;
        }
    }

    static resultTournament(data) {
        let tornamentInfo = {};
        let promiseList = [
            dbTornament.selectById(data.tournamentId),
            dbPlayer.selectPlayersByTournament(data.tournamentId),
            dbPlayer.selectBackersByTournament(data.tournamentId)
        ];
        return Promise.all(promiseList).then(results => {
            tornamentInfo.tournament = results[0];
            tornamentInfo.players = results[1];
            tornamentInfo.backers = results[2];
            TournamentLogic.validateResultTornament(tornamentInfo, data);
            tornamentInfo.winners = TournamentLogic.getWinnerPlayers(tornamentInfo, data);
            return dbTornament.closeTournament(tornamentInfo, db);
        }).then(r => {
            return Promise.resolve("ok");
        }).catch(err => {
            return Promise.reject(err);
        });
    }

    static getWinnerPlayers(ti, request) {
        let result = [];
        request.winners.forEach((w) => {
            let winners = []
            let winner = ti.players.filter(x => x.player_id == w.playerId)[0];
            let winnerBackers = ti.backers.filter(e => e.tournament_player_id == winner.id);
            let dividedCount = winnerBackers.length + 1;
            winners.push({ id: winner.player_id, rate: w.prize / dividedCount });
            winnerBackers.forEach(b => {
                winners.push({ id: b.player_id, rate: w.prize / dividedCount });
            });
            result.push(...winners);
        });
        return result;
    }

    static validateResultTornament(ti, request) {
        if (!ti.tournament) throw new AppError(404, errMsg.tournamentIdNotFound);
        if (!ti.tournament.is_active) throw new AppError(400, errMsg.tournamentEnded);
        request.winners.forEach((w) => {
            var winner = ti.players.filter(x => x.player_id == w.playerId)[0];
            if (!winner) throw new AppError(400, errMsg.plaerNotParticipate + ' player ' + w.playerId);
        });

    }
}


module.exports = TournamentLogic;