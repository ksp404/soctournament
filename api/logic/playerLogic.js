"use strict";
const AppError = require('../error/index');
const db = require('../db').db.Player;
const errMsg = require('../error/errorMessage');

class PlayerLogic {

    static fund(id, points) {
        return db.create(id, points)
                .then((p) => {
                    return Promise.resolve(p);
                }).catch((err) => {
                    return Promise.reject(err);
                });
    }

    static take(id, points) {
        return db.changePoints(id, -points).then((player) => {
            if (player) {
                return Promise.resolve(player);
            } else {
                throw new AppError(404, errMsg.playerNotFound);
            }
        }).catch(err => {
            if (err.constraint === errMsg.dbMsgConstraintLess0)
                err = new AppError(400, errMsg.notEnoughPoints);
            return Promise.reject(err);
        });
    }

    static balance(id) {
        return db.selectById(id).then(p => {
            if (!p) throw new AppError(404, errMsg.playerNotFound);
            var res = {
                playerId: p.id,
                balance: p.points
            }
            return Promise.resolve(res);
        }).catch(err => {
            return Promise.reject(err);
        });
    }
}


module.exports = PlayerLogic;