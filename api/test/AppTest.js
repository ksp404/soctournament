const app = require('../app');
const supertest = require('supertest');
const assert = require('assert');
const db = require('../db').db;
const errMsg = require('./../error/errorMessage');
describe('appTest', () => {
    before((done) => {
        db.db.init().then(result => {
            return db.db.resetDb();
        }).then(r => {
            return Promise.all([db.Player.create('take', 300),
            db.Player.create('balance', 300),
            db.Tournament.create('exists', 300),
            db.Tournament.create('join', 100),
            db.Player.create('j1', 100),
            db.Player.create('j2', 50)]);
        }).then((res)=>{
            done();
        }).catch(err => {
            return err
        });
    });

    it('fund', (done) => {
        let points = 300;
        supertest(app)
            .get('/fund?playerid=1&points=' + points)
            .expect(200)
            .end((err, res) => {
                assert.equal(res.status, 200);
                assert.equal(res.body.id, 1);
                assert.ok(res.body.points == points)
                done();
            })
    })

    it('fund bad request', (done) => {
        let points = 300;
        supertest(app)
            .get('/fund?playd=1&points=' + points)
            .expect(400)
            .end((err, res) => {
                assert.equal(res.status, 400)
                done();
            })
    })

    it('take', (done) => {
        supertest(app)
            .get('/take?playerid=take&points=300')
            .expect(200)
            .end((err, res) => {
                assert.equal(res.status, 200);
                assert.ok(res.body.points >= 0)
                done();
            })
    })


    it('take bad request', (done) => {
        supertest(app)
            .get('/take?playerid=666&points=50')
            .expect(404)
            .end((err, res) => {
                assert.equal(res.text, errMsg.playerNotFound)
                assert.equal(res.status, 404);
                done();
            })
    })

    it('take more points', (done) => {
        supertest(app)
            .get('/take?playerid=take&points=500000')
            .expect(400)
            .end((err, res) => {
                assert.equal(res.text, errMsg.notEnoughPoints)
                assert.equal(res.status, 400);
                done();
            })
    })

    it('balance', (done) => {
        supertest(app)
            .get('/balance?playerid=balance')
            .expect(200)
            .end((err, res) => {
                assert.equal(res.status, 200);
                assert.equal(res.body.playerId, 'balance')
                assert.equal(res.body.balance, '300')
                done();
            })
    })

    it('anonce tournament', (done) => {
        supertest(app)
            .get('/announceTournament?tournamentid=1&deposit=300')
            .expect(200)
            .end((err, res) => {
                assert.equal(res.status, 200);
                assert.equal(res.body.id, '1')
                assert.equal(res.body.deposit, 300)
                done();
            })
    })

    it('anonce tournament exists', (done) => {
        supertest(app)
            .get('/announceTournament?tournamentid=exists&deposit=300')
            .expect(400)
            .end((err, res) => {
                assert.equal(res.status, 400);
                assert.equal(res.text, errMsg.tournamentExists)
                done();
            })
    })

    it('join tournament onePlayer', (done) => {
        supertest(app)
            .get('/joinTournament?tournamentid=join&playerid=j1')
            .expect(200)
            .end((err, res) => {
                assert.equal(res.status, 200);
                done();
            })
    })

    it('join tournament not have enough points', (done) => {
        supertest(app)
            .get('/joinTournament?tournamentid=join&playerid=j2')
            .expect(400)
            .end((err, res) => {
                assert.equal(res.status, 400);
                done();
            })
    })

})