"use strict";

const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const AppError = require('./error/index');
const app = express();
const log = require('./log')(module);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.use((req, res, next) => {
    for (var key in req.query) {
        req.query[key.toLowerCase()] = req.query[key];
    }
    next();
});
//app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.disable('etag');

require('./routes')(app);

app.use((req, res, next) => {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use((err, req, res, next) => {
    if (typeof err === 'number') {
        err = new AppError(err);
    }

    if (err instanceof AppError) {
        log.debug(err);
        res.status(err.status);
        res.end(err.message);
    } else {
        log.error(err);
        res.status(err.status || 400);
        res.send(err);
    }
});


module.exports = app;
